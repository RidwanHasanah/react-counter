import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Hello</h1>
        <Counter nama="Indoesia" />
        <Counter nama="Brasil" />
        <Counter nama="English" />
      </div>
    );
  }
}

export default App;

class Counter extends Component {
  state = {
    number: 0
  };
  naik = () => {
    this.setState({
      number: this.state.number + 1
    });
  };
  turun = () => {
    this.setState({
      number: this.state.number - 1
    });
  };
  render() {
    return (
      <div>
        <h1>Ranking {this.props.nama}</h1>
        <h2>{this.state.number}</h2>
        <button onClick={this.naik}>naik</button>
        <button onClick={this.turun}>turun</button>
      </div>
    );
  }
}
